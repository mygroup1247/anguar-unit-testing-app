import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {ProductFiltersComponent} from "./components/filters/product-filters.component";
import {ProductStore} from "./services/ProductStore";
import {ListComponent} from "./components/list/list.component";
import {FilterComponentComponent} from "./components/filters/filter-component/filter-component.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ProductSpecsPipe} from "./pipes/ProductSpecsPipe";

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ProductFiltersComponent,
        ListComponent,
        FilterComponentComponent,
        ProductSpecsPipe
      ],
      imports: [
        BrowserAnimationsModule
      ],
      providers: [
        {provide: ProductStore, useClass: ProductStore}
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
