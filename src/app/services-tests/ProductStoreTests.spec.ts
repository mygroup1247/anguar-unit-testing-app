import {
  IMonitor,
  IMonitorFilter,
  IProductFilter,
  MatrixSpicesEnum,
  MonitorResolutionsEnum,
  ProductStore
} from "../services/ProductStore";

describe('ProductStore tests', () => {
  let productStore: ProductStore;
  beforeEach(() => { productStore = new ProductStore(); });

  const products: IMonitor[] = [
    {id: 1, title: 'Asus FX1', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
    {id: 2, title: 'Asus FX2', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.VA},
    {id: 3, title: 'Asus FX3', brand: 'Asus', diagonal: 24, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
    {id: 4, title: 'Asus FX4', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
    {id: 5, title: 'Asus FX5', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.VA},
    {id: 6, title: 'Asus FF6', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
    {id: 7, title: 'Asus FF7', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.IPS},
    {id: 8, title: 'Asus FF8', brand: 'Asus', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
    {id: 9, title: 'Asus FF9', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
    {id: 10, title: 'Asus FS1', brand: 'Asus', diagonal: 24, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
    {id: 11, title: 'Asus FS2', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
    {id: 12, title: 'Asus FS3', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
    {id: 13, title: 'Lenovo SL1', brand: 'Lenovo', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
    {id: 14, title: 'Lenovo SL2', brand: 'Lenovo', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.VA},
    {id: 15, title: 'Lenovo SL3', brand: 'Lenovo', diagonal: 24, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
    {id: 16, title: 'Lenovo SL4', brand: 'Lenovo', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
    {id: 17, title: 'Lenovo SL5', brand: 'Lenovo', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.VA},
    {id: 18, title: 'Lenovo SG1', brand: 'Lenovo', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
    {id: 19, title: 'Lenovo SG2', brand: 'Lenovo', diagonal: 32, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.IPS},
    {id: 20, title: 'Lenovo SG3', brand: 'Lenovo', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
    {id: 21, title: 'Lenovo SG4', brand: 'Lenovo', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
    {id: 22, title: 'Lenovo SG5', brand: 'Lenovo', diagonal: 24, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
    {id: 23, title: 'Lenovo GA1', brand: 'Lenovo', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
    {id: 24, title: 'Lenovo GA2', brand: 'Lenovo', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
    {id: 25, title: 'HP SL1', brand: 'HP', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
    {id: 26, title: 'HP SL2', brand: 'HP', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.VA},
    {id: 27, title: 'HP SL3', brand: 'HP', diagonal: 24, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
    {id: 28, title: 'HP SL4', brand: 'HP', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
    {id: 29, title: 'HP SL5', brand: 'HP', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.VA},
    {id: 30, title: 'HP SG1', brand: 'HP', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
    {id: 31, title: 'HP SG2', brand: 'HP', diagonal: 32, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.IPS},
    {id: 32, title: 'HP SG3', brand: 'HP', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
    {id: 33, title: 'HP SG4', brand: 'HP', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
    {id: 34, title: 'HP SG5', brand: 'HP', diagonal: 24, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
    {id: 35, title: 'HP GA1', brand: 'HP', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
    {id: 36, title: 'HP GA2', brand: 'HP', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
  ];

  it ('getProducts should return all elements', () => {
    expect(productStore.getProducts()).toEqual(products);
  });

  it ('getProductsById should return element with id 1', () => {
    const productId = 1;
    const expectedProduct = {id: 1, title: 'Asus FX1', brand: 'Asus',
      diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA};
    expect(productStore.getProductById(productId)).toEqual(expectedProduct);
  });

  it ('getFilteredProducts should return products considering filters', () => {
    const filteringByBrandExpectedResult: IMonitor[] = [
      {id: 1, title: 'Asus FX1', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"],
        matrix: MatrixSpicesEnum.VA},
      {id: 2, title: 'Asus FX2', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.VA},
      {id: 3, title: 'Asus FX3', brand: 'Asus', diagonal: 24, resolution: MonitorResolutionsEnum["3840x2160"],
        matrix: MatrixSpicesEnum.IPS},
      {id: 4, title: 'Asus FX4', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"],
        matrix: MatrixSpicesEnum.IPS},
      {id: 5, title: 'Asus FX5', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"],
        matrix: MatrixSpicesEnum.VA},
      {id: 6, title: 'Asus FF6', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"],
        matrix: MatrixSpicesEnum.IPS},
      {id: 7, title: 'Asus FF7', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["1920x1080"],
        matrix: MatrixSpicesEnum.IPS},
      {id: 8, title: 'Asus FF8', brand: 'Asus', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 9, title: 'Asus FF9', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 10, title: 'Asus FS1', brand: 'Asus', diagonal: 24, resolution: MonitorResolutionsEnum["2560x1440"],
        matrix: MatrixSpicesEnum.IPS},
      {id: 11, title: 'Asus FS2', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"],
        matrix: MatrixSpicesEnum.VA},
      {id: 12, title: 'Asus FS3', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"],
        matrix: MatrixSpicesEnum.IPS}];
    const filtersByBrand: IProductFilter = {brands: ['Asus']};
    const filteringByResolutionExpectedResult: IMonitor[] = [
      {id: 2, title: 'Asus FX2', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.VA},
      {id: 8, title: 'Asus FF8', brand: 'Asus', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 9, title: 'Asus FF9', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 14, title: 'Lenovo SL2', brand: 'Lenovo', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.VA},
      {id: 20, title: 'Lenovo SG3', brand: 'Lenovo', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 21, title: 'Lenovo SG4', brand: 'Lenovo', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 26, title: 'HP SL2', brand: 'HP', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.VA},
      {id: 32, title: 'HP SG3', brand: 'HP', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 33, title: 'HP SG4', brand: 'HP', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN}
    ];
    const filtersByResolution: IMonitorFilter = {resolutions: [MonitorResolutionsEnum['1280x720']]};
    const filteringByMatrixExpectedResult: IMonitor[] = [
      {id: 8, title: 'Asus FF8', brand: 'Asus', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 9, title: 'Asus FF9', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 20, title: 'Lenovo SG3', brand: 'Lenovo', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 21, title: 'Lenovo SG4', brand: 'Lenovo', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 32, title: 'HP SG3', brand: 'HP', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 33, title: 'HP SG4', brand: 'HP', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN}
    ];
    const filtersByMatrix: IMonitorFilter = {matrix: [MatrixSpicesEnum.TN]};
    const filteringByDiagonalExpectedResult: IMonitor[] = [
      {id: 8, title: 'Asus FF8', brand: 'Asus', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 20, title: 'Lenovo SG3', brand: 'Lenovo', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 32, title: 'HP SG3', brand: 'HP', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
    ];
    const filtersByDiagonal: IMonitorFilter = {diagonal: [22]};
    const filteringByResolutionMatrixBrandDiagonalExpectedResult: IMonitor[] = [
      {id: 8, title: 'Asus FF8', brand: 'Asus', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN},
      {id: 32, title: 'HP SG3', brand: 'HP', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'],
        matrix: MatrixSpicesEnum.TN}
    ];
    const filterByResolutionMatrixBrandDiagonal: IMonitorFilter = {resolutions: [MonitorResolutionsEnum['1280x720']],
      matrix: [MatrixSpicesEnum.TN], brands: ['Asus', 'HP'], diagonal: [22]};

    expect(productStore.getFilteredProducts(filtersByBrand)).toEqual(filteringByBrandExpectedResult);
    expect(productStore.getFilteredProducts(filtersByDiagonal)).toEqual(filteringByDiagonalExpectedResult);
    expect(productStore.getFilteredProducts(filtersByResolution)).toEqual(filteringByResolutionExpectedResult);
    expect(productStore.getFilteredProducts(filtersByMatrix)).toEqual(filteringByMatrixExpectedResult);
    expect(productStore.getFilteredProducts(filterByResolutionMatrixBrandDiagonal))
      .toEqual(filteringByResolutionMatrixBrandDiagonalExpectedResult);
  });
});
