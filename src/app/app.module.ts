import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ListComponent } from './components/list/list.component';
import {ProductFiltersComponent} from "./components/filters/product-filters.component";
import {ProductStore} from "./services/ProductStore";
import {ProductSpecsPipe} from "./pipes/ProductSpecsPipe";
import { FilterComponentComponent } from './components/filters/filter-component/filter-component.component';
import {BrowserAnimationsModule, NoopAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
      AppComponent,
      ProductFiltersComponent,
      ListComponent,
      ProductFiltersComponent,
      ProductSpecsPipe,
      FilterComponentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [ProductStore],
  bootstrap: [AppComponent]
})
export class AppModule { }
