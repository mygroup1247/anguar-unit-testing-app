import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {IProduct, ProductStore} from "../../services/ProductStore";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {
  products: IProduct[];
  filterSubscription$: Subscription;

  constructor(private readonly productStore: ProductStore) {
  }

  ngOnInit(): void {
    this.products = this.productStore.getProducts();
    this.filterSubscription$ = this.productStore.filters$.subscribe(response => {
      this.products = this.productStore.getFilteredProducts(response);
    });
  }

  ngOnDestroy(): void {
    this.filterSubscription$.unsubscribe();
  }

}
