import {ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { ListComponent } from './list.component';
import {ProductSpecsPipe} from "../../pipes/ProductSpecsPipe";
import {IMonitor, MatrixSpicesEnum, MonitorResolutionsEnum, ProductStore} from "../../services/ProductStore";

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListComponent, ProductSpecsPipe ],
      providers: [{provide: ProductStore, useClass: ProductStore}]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it ('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('ngOnInit receive filtered products', inject([ProductStore], (productStore) => {
    const expectedProducts: IMonitor[] = [
      {id: 1, title: 'Asus FX1', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
      {id: 2, title: 'Asus FX2', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.VA},
      {id: 3, title: 'Asus FX3', brand: 'Asus', diagonal: 24, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 4, title: 'Asus FX4', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 5, title: 'Asus FX5', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.VA},
      {id: 6, title: 'Asus FF6', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
      {id: 7, title: 'Asus FF7', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.IPS},
      {id: 8, title: 'Asus FF8', brand: 'Asus', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
      {id: 9, title: 'Asus FF9', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
      {id: 10, title: 'Asus FS1', brand: 'Asus', diagonal: 24, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
      {id: 11, title: 'Asus FS2', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
      {id: 12, title: 'Asus FS3', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS}
    ];
    const brandForFiltering = 'Asus';
    productStore.filters$.next({brands: [brandForFiltering]});
    expect(component.products).toEqual(expectedProducts);
  }));
});
