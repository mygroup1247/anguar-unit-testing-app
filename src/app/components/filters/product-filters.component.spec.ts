import {ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { ProductFiltersComponent } from './product-filters.component';
import {IMonitorFilter, ProductStore} from "../../services/ProductStore";
import {FilterComponentComponent, SelectItem} from "./filter-component/filter-component.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe('FiltersComponent', () => {
  let component: ProductFiltersComponent;
  let fixture: ComponentFixture<ProductFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductFiltersComponent, FilterComponentComponent ],
      imports: [
        BrowserAnimationsModule
      ],
      providers: [ {provide: ProductStore, useClass: ProductStore} ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it ('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('filtersValueChange should change filters$ variable in ProductStore', inject([ProductStore],
    (productStore) => {
      const dataType = 'Brand';
      const items: SelectItem[] = [{id: 1, title: 'Asus', isSelected: true}];
      const expectedResult = {
        brands: ['Asus'],
        titles: [], resolutions: [],
        diagonal: [], matrix: []} as IMonitorFilter;
      component.filtersValueChange({elements: items, dataType});
      expect(productStore.filters$.value).toEqual(expectedResult);
  }));
});
