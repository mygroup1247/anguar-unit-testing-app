import {Component, ElementRef, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {delay, Subject} from "rxjs";

@Component({
  selector: 'app-filter-component',
  templateUrl: './filter-component.component.html',
  styleUrls: ['./filter-component.component.css'],
  animations: [
    trigger('rotateArrow', [
      state('down', style({transform: 'rotate(45deg)'})),
      state('up', style({transform: 'rotate(-135deg)'})),
      transition('down <=> up', [animate('0.3s ease-in')] )
    ]),
    trigger('expandSelectOptions', [
      transition(':enter', [
        style({height: '0'}),
        animate('0.3s ease-in', style({height: '*'}))
      ]),
      transition(':leave', [
        style({height: '*'}),
        animate('0.3s ease-in', style({height: '0'}))
      ])
    ]),
    trigger('fade', [
      transition(':enter', [
        style({opacity: '0'}),
        animate('0.3s ease-in', style({opacity: '1'}))
      ]),
      transition(':leave', [
        style({opacity: '1'}),
        animate('0.3s ease-in', style({opacity: '0'}))
      ])
    ])
  ]
})
export class FilterComponentComponent implements OnInit, OnDestroy{
  @Input() filterData: SelectData;
  @Input() resetFilter$: Subject<boolean>;
  @Output() changeSelectedItems = new EventEmitter<{elements: SelectItem[], dataType: string}>();
  selectedItems: SelectItem[] = [];
  selectedItemsTitle = '';
  isFilterOpened = false;
  emitSelectedItemsWithDelay = new Subject<boolean>();

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit(): void {
    this.resetFilter$.subscribe(() => {
      const needToBeEmit = this.selectedItems.length > 0;
      this.selectedItems = [];
      this.selectedItemsTitle = '';
      this.filterData.items.forEach(el => {
        el.isSelected = false;
      });
      if (needToBeEmit) {
        this.emitSelectedItemsWithDelay.next(true);
      }
    });
    this.emitSelectedItemsWithDelay.pipe(delay(400)).subscribe(() => {
      this.changeSelectedItems.emit({elements: this.selectedItems, dataType: this.filterData.dataType});
    })
  }

  selectItem(item: SelectItem): void {
    const selectedItem = this.selectedItems.find(el => el.id === item.id);
    if (selectedItem) {
      const index = this.selectedItems.indexOf(selectedItem);
      item.isSelected = false;
      this.selectedItems.splice(index, 1);
      this.removeTitleFromSelectItemsTitle(item.title);
    } else {
      this.selectedItems.push(item);
      item.isSelected = true;
      this.addTitleToSelectItemsTitle(item.title);
    }
    this.emitSelectedItemsWithDelay.next(true);
  }

  private addTitleToSelectItemsTitle(title: string): void {
    if (this.selectedItemsTitle.length === 0) {
      this.selectedItemsTitle += title;
    } else {
      this.selectedItemsTitle += ', ' + title;
    }
  }

  private removeTitleFromSelectItemsTitle(title: string): void {
    if (this.selectedItemsTitle.length === 1 ) {
      this.selectedItemsTitle = '';
    } else {
      const splittedTitles = this.selectedItemsTitle.split(', ');
      const titleFromSplittedTitles = splittedTitles.find(el => el === title);
      if (titleFromSplittedTitles) {
        splittedTitles.splice(splittedTitles.indexOf(titleFromSplittedTitles), 1);
      }
      this.selectedItemsTitle = '';
      splittedTitles.forEach(el => {
        this.addTitleToSelectItemsTitle(el);
      });
    }
  }

  @HostListener('document:click', ['$event'])
  private onDocumentClick(event: MouseEvent): void {
    event.stopPropagation();
    const clickedInside = this.elementRef.nativeElement.contains(event.target);
    if (!clickedInside) {
      this.isFilterOpened = false;
    }
  }

  ngOnDestroy(): void {
    this.emitSelectedItemsWithDelay.unsubscribe();
    this.resetFilter$.unsubscribe()
  }

}

export interface SelectItem {
  id: number;
  title: string;
  isSelected: boolean;
}

export interface SelectData {
  dataType: string;
  selectTitle: string;
  items: SelectItem[];
}
