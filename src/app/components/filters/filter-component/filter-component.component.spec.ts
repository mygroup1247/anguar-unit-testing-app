import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FilterComponentComponent, SelectData, SelectItem} from './filter-component.component';
import {ProductStore} from "../../../services/ProductStore";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {Subject} from "rxjs";

describe('FilterComponentComponent', () => {
  let component: FilterComponentComponent;
  let fixture: ComponentFixture<FilterComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterComponentComponent ],
      imports: [BrowserAnimationsModule],
      providers: [{providers: ProductStore, useClass: ProductStore}]
    })
    .compileComponents();
    jasmine.clock().install();

    fixture = TestBed.createComponent(FilterComponentComponent);
    component = fixture.debugElement.componentInstance;
    const resetFiltersSubject$ = new Subject<boolean>();
    const filterData: SelectData = {
      dataType: 'Asus',
      items: [
        {id: 1, title: 'Asus BB', isSelected: false},
        {id: 2, title: 'Asus B6', isSelected: false}
      ],
      selectTitle: 'Asus'
    };
    component.filterData = filterData;
    component.resetFilter$ = resetFiltersSubject$;
    fixture.detectChanges();
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  it ('should create', () => {
      expect(component).toBeTruthy();
  });

  it ('ngOnInit, test resetFilters$ without emit', () => {
      spyOn(component.changeSelectedItems, 'emit');
      component.resetFilter$.next(true);
      const expectedSelectedItems: SelectItem[] = [];
      const expectedSelectedItemsTitle = '';

      expect(component.selectedItems).toEqual(expectedSelectedItems);
      expect(component.selectedItemsTitle).toEqual(expectedSelectedItemsTitle);
      fixture.detectChanges();
      expect(component.changeSelectedItems.emit).toHaveBeenCalledTimes(0);
  });

  it ('ngOnInit, test resetFilters$ with emit should change selectedItems to empty array and selectedItemsTitle to ' +
    'empty string and after 400ms delay emit value',
    fakeAsync(() => {
      spyOn(component.changeSelectedItems, 'emit');
      component.selectedItems = [{id: 1, title: 'Asus BB', isSelected: false}];
      component.resetFilter$.next(true);
      const expectedSelectedItems: SelectItem[] = [];
      const expectedSelectedItemsTitle = '';
      const expectedEmitData = {elements: [], dataType: 'Asus'};
      const delayBeforeEmit = 400;

      expect(component.changeSelectedItems.emit).toHaveBeenCalledTimes(0);
      tick(delayBeforeEmit);
      fixture.detectChanges();
      expect(component.selectedItems).toEqual(expectedSelectedItems);
      expect(component.selectedItemsTitle).toEqual(expectedSelectedItemsTitle);
      expect(component.changeSelectedItems.emit).toHaveBeenCalledTimes(1);
      expect(component.changeSelectedItems.emit).toHaveBeenCalledWith(expectedEmitData);
  }));

  it ('ngOnInit, test emitSelectedItemsWithDelay should emit value after delay 400ms', fakeAsync(() => {
      spyOn(component.changeSelectedItems, 'emit');
      const expectedItem = {id: 1, title: 'Asus BB', isSelected: false};
      const expectedEmitData = {elements: [expectedItem], dataType: 'Asus'};
      const delayBeforeEmit = 400;
      component.selectItem(expectedItem);
      fixture.detectChanges();

      expect(component.changeSelectedItems.emit).toHaveBeenCalledTimes(0);
      tick(delayBeforeEmit);
      expect(component.changeSelectedItems.emit).toHaveBeenCalledOnceWith(expectedEmitData);
  }));

  it ('selectItem, select not selected item should add item to selectedItems array and add title to selectedItemsTitle',
    () => {
      const expectedItem = {id: 1, title: 'Asus BB', isSelected: false};
      const expectedSelectedItems = [expectedItem];
      const expectedSelectedItemsTitle = 'Asus BB';
      component.selectItem(expectedItem);
      expect(component.selectedItems).toEqual(expectedSelectedItems);
      expect(component.selectedItemsTitle).toEqual(expectedSelectedItemsTitle);
    });

  it ('selectItem, select selected item should remove item to selectedItems array and remove title ' +
    'to selectedItemsTitle', () => {
      const selectedItem = {id: 1, title: 'Asus BB', isSelected: false};
      const expectedSelectedItems = [];
      const expectedSelectedItemsTitles = '';
      component.selectedItems.push(selectedItem);
      component.selectedItemsTitle = `${selectedItem.title}`;
      component.selectItem(selectedItem);
      expect(component.selectedItemsTitle).toEqual(expectedSelectedItemsTitles);
      expect(component.selectedItems).toEqual(expectedSelectedItems);
  });

  it ('selectItem, select not selected item should add item to selectedItems and ' +
    'add title to selectedItemsTitle, if selectedItemsTitle not empty string and selectedItems contain items', () => {
      const selectedItem = {id: 1, title: 'Asus BB', isSelected: false};
      const newItem = {id: 2, title: 'Asus B6', isSelected: false};
      const expectedSelectedItems = [selectedItem, newItem];
      const expectedSelectedItemsTitle = 'Asus BB, Asus B6';
      component.selectedItems.push(selectedItem);
      component.selectedItemsTitle = 'Asus BB';
      component.selectItem(newItem);
      expect(component.selectedItems).toEqual(expectedSelectedItems);
      expect(component.selectedItemsTitle).toEqual(expectedSelectedItemsTitle);
    }
  );

  it ('selectItem, select selected item should remove item from selectedItems and ' +
    'remove title from selectedItemsTitle, if selectedItemsTitle not empty string and selectedItems contain items',
    () => {
      const selectedItem = {id: 1, title: 'Asus BB', isSelected: false};
      const selectedItem2 = {id: 2, title: 'Asus B6', isSelected: false};
      const expectedSelectedItems = [selectedItem];
      const expectedSelectedItemsTitle = 'Asus BB';
      component.selectedItems.push(selectedItem);
      component.selectedItems.push(selectedItem2);
      component.selectedItemsTitle = 'Asus BB, Asus B6';
      component.selectItem(selectedItem2);
      expect(component.selectedItems).toEqual(expectedSelectedItems);
      expect(component.selectedItemsTitle).toEqual(expectedSelectedItemsTitle);
    })
});
