import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  IMonitor,
  IMonitorFilter,
  MatrixSpicesEnum,
  MonitorResolutionsEnum,
  ProductStore
} from "../../services/ProductStore";
import {SelectData, SelectItem} from "./filter-component/filter-component.component";
import {Subject, Subscription} from "rxjs";

@Component({
  selector: 'app-filters',
  templateUrl: './product-filters.component.html',
  styleUrls: ['./product-filters.component.css'],
})
export class ProductFiltersComponent implements OnInit, OnDestroy {
  brandsData: SelectData = {
    dataType: 'Brand',
    selectTitle: 'Brand',
    items: []
  };
  monitorResolutionsData: SelectData = {
    dataType: 'Resolution',
    selectTitle: 'Resolution',
    items: []
  };
  matrixSpicesData: SelectData = {
    dataType: 'Matrix',
    selectTitle: 'Matrix',
    items: []
  };
  diagonalsData: SelectData = {
    dataType: 'Diagonal',
    selectTitle: 'Diagonal',
    items: []
  };
  resetFilter$ = new Subject<boolean>();
  filtersValue: IMonitorFilter;
  filterSubscription$: Subscription;

  constructor(private readonly productStore: ProductStore) {
  }

  ngOnInit(): void {
    this.initializeFilterData();
     this.filterSubscription$ = this.productStore.filters$.subscribe(response => {
      this.filtersValue = response;
    });
  }

  private initializeFilterData(): void {
    const products = this.productStore.getProducts() as IMonitor[];
    let idCounter = 0;
    this.brandsData.items.push(...[...(new Set(products.map(el => el.brand)))]
      .map(el => {idCounter++; return {isSelected: false, title: el, id: idCounter}}));
    idCounter = 0;
    this.diagonalsData.items.push(...[...(new Set(products.map(el => el.diagonal)))]
      .map(el => {idCounter++; return {isSelected: false, title: el.toString(), id: idCounter}}));
    idCounter = 0;
    this.matrixSpicesData.items.push(...[...(new Set(products.map(el => MatrixSpicesEnum[el.matrix])))]
      .map(el => {idCounter++; return {isSelected: false, title: el, id: idCounter}}));
    idCounter = 0;
    this.monitorResolutionsData.items.push(...[...(new Set(products.map(el => MonitorResolutionsEnum[el.resolution])))]
      .map(el => {idCounter++; return {isSelected: false, title: el, id: idCounter}}));
  }

  filtersValueChange(data: {elements: SelectItem[], dataType: string}): void {
    switch (data.dataType) {
      case 'Brand':
        this.filtersValue.brands = data.elements.map(el => el.title);
        break;
      case 'Resolution':
        this.filtersValue.resolutions = data.elements.map(el => MonitorResolutionsEnum[el.title]);
        break;
      case 'Matrix':
        this.filtersValue.matrix = data.elements.map(el => MatrixSpicesEnum[el.title]);
        break;
      case 'Diagonal':
        this.filtersValue.diagonal = data.elements.map(el => Number.parseInt(el.title));
        break;
    }
    this.productStore.filters$.next(this.filtersValue);
  }

  ngOnDestroy(): void {
    this.filterSubscription$.unsubscribe();
  }

}
