import {Pipe, PipeTransform} from "@angular/core";
import {IProduct, MatrixSpicesEnum, MonitorResolutionsEnum} from "../services/ProductStore";

@Pipe({name: 'productSpecs'})
export class ProductSpecsPipe implements PipeTransform {
  transform(value: IProduct): string {
    let result: string = '';
    Object.entries(value).map( el => {
      if (el[0] !== 'title' && el[0] !== 'id') {
        if (el[0] == 'resolution') {
          result+= el[0] + ': ' + MonitorResolutionsEnum[el[1]] + '<br>';
        } else if (el[0] == 'matrix') {
          result+= el[0] + ': ' + MatrixSpicesEnum[el[1]] + '<br>';
        } else {
          result+= el[0] + ': ' + el[1] + '<br>';
        }
      }
    })
    return result;
  }

}
