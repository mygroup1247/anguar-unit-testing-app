import {Injectable} from "@angular/core";
import {BehaviorSubject, filter, Observable, Subject} from "rxjs";

@Injectable()
export class ProductStore {
  private products: IProduct[];
  public filters$ = new BehaviorSubject<IProductFilter>({
    brands: [],
    titles: [], resolutions: [],
    diagonal: [], matrix: []} as IMonitorFilter);

  constructor() {
    this.products = this.initializeProductsMock();
  }

  private initializeProductsMock(): IProduct[] {
    const products: IMonitor[] = [
      {id: 1, title: 'Asus FX1', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
      {id: 2, title: 'Asus FX2', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.VA},
      {id: 3, title: 'Asus FX3', brand: 'Asus', diagonal: 24, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 4, title: 'Asus FX4', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 5, title: 'Asus FX5', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.VA},
      {id: 6, title: 'Asus FF6', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
      {id: 7, title: 'Asus FF7', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.IPS},
      {id: 8, title: 'Asus FF8', brand: 'Asus', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
      {id: 9, title: 'Asus FF9', brand: 'Asus', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
      {id: 10, title: 'Asus FS1', brand: 'Asus', diagonal: 24, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
      {id: 11, title: 'Asus FS2', brand: 'Asus', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
      {id: 12, title: 'Asus FS3', brand: 'Asus', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 13, title: 'Lenovo SL1', brand: 'Lenovo', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
      {id: 14, title: 'Lenovo SL2', brand: 'Lenovo', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.VA},
      {id: 15, title: 'Lenovo SL3', brand: 'Lenovo', diagonal: 24, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 16, title: 'Lenovo SL4', brand: 'Lenovo', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 17, title: 'Lenovo SL5', brand: 'Lenovo', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.VA},
      {id: 18, title: 'Lenovo SG1', brand: 'Lenovo', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
      {id: 19, title: 'Lenovo SG2', brand: 'Lenovo', diagonal: 32, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.IPS},
      {id: 20, title: 'Lenovo SG3', brand: 'Lenovo', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
      {id: 21, title: 'Lenovo SG4', brand: 'Lenovo', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
      {id: 22, title: 'Lenovo SG5', brand: 'Lenovo', diagonal: 24, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
      {id: 23, title: 'Lenovo GA1', brand: 'Lenovo', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
      {id: 24, title: 'Lenovo GA2', brand: 'Lenovo', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 25, title: 'HP SL1', brand: 'HP', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
      {id: 26, title: 'HP SL2', brand: 'HP', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.VA},
      {id: 27, title: 'HP SL3', brand: 'HP', diagonal: 24, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 28, title: 'HP SL4', brand: 'HP', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
      {id: 29, title: 'HP SL5', brand: 'HP', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.VA},
      {id: 30, title: 'HP SG1', brand: 'HP', diagonal: 27, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
      {id: 31, title: 'HP SG2', brand: 'HP', diagonal: 32, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.IPS},
      {id: 32, title: 'HP SG3', brand: 'HP', diagonal: 22, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
      {id: 33, title: 'HP SG4', brand: 'HP', diagonal: 23, resolution: MonitorResolutionsEnum['1280x720'], matrix: MatrixSpicesEnum.TN},
      {id: 34, title: 'HP SG5', brand: 'HP', diagonal: 24, resolution: MonitorResolutionsEnum["2560x1440"], matrix: MatrixSpicesEnum.IPS},
      {id: 35, title: 'HP GA1', brand: 'HP', diagonal: 27, resolution: MonitorResolutionsEnum["1920x1080"], matrix: MatrixSpicesEnum.VA},
      {id: 36, title: 'HP GA2', brand: 'HP', diagonal: 32, resolution: MonitorResolutionsEnum["3840x2160"], matrix: MatrixSpicesEnum.IPS},
    ];
    return products;
  }


  public getProducts(): IProduct[] {
    return this.products;
  }

  public getProductById(id: number): IProduct | null {
    const product = this.products.find(product => product.id === id);
    if (product) {
      return product;
    }
    return null;
  }

  public getFilteredProducts(filters: IProductFilter): IProduct[] {
    let filterResult = this.products;
    const monitorFilters = filters as IMonitorFilter;
    if (filters.brands?.length > 0) {
      filterResult = this.filterByBrand(filterResult, filters.brands);
    }
    if (monitorFilters.resolutions?.length > 0) {
      filterResult = this.filterByResolution(filterResult as IMonitor[], monitorFilters.resolutions);
    }
    if (monitorFilters.diagonal?.length > 0) {
      filterResult = this.filterByDiagonal(filterResult as IMonitor[], monitorFilters.diagonal);
    }
    if (monitorFilters.matrix?.length > 0) {
      filterResult = this.filterByMatrix(filterResult as IMonitor[], monitorFilters.matrix);
    }
    return  filterResult;

  }

  private filterByBrand(products: IProduct[], brands: string[]): IProduct[] {
    return products.filter(el => brands.includes(el.brand));
  }

  private filterByMatrix(products: IMonitor[], matrix: MatrixSpicesEnum[]): IProduct[] {
    return products.filter(el => matrix.includes(el.matrix));
  }

  private filterByResolution(products: IMonitor[], resolutions: MonitorResolutionsEnum[]): IProduct[] {
    return products.filter(el => resolutions.includes(el.resolution));
  }

  private filterByDiagonal(products: IMonitor[], diagonal: number[]): IProduct[] {
    return products.filter(el => diagonal.includes(el.diagonal));
  }

}



export interface IProduct {
  id: number;
  title: string;
  brand: string;
}

export interface IMonitor extends IProduct {
  diagonal: number;
  resolution: MonitorResolutionsEnum;
  matrix: MatrixSpicesEnum;
}

export interface IProductFilter {
  titles?: string[];
  brands?: string[];
}

export interface IMonitorFilter extends IProductFilter {
  diagonal?: number[];
  resolutions?: MonitorResolutionsEnum[];
  matrix?: MatrixSpicesEnum[];
}

export enum MonitorResolutionsEnum {
  '1280x720',
  '1920x1080',
  '2560x1440',
  '3840x2160'
}

export enum MatrixSpicesEnum {
  'IPS',
  'VA',
  'TN'
}

